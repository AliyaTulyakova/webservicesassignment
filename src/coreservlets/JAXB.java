package coreservlets;

import java.io.File;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.google.gson.Gson;

public class JAXB {
	
    public static void main(String[] args){
    	bookDAO dao=new bookDAO();
    	BookInfo bookInfo=new BookInfo();
    	Gson gson = new Gson();
   
    	ArrayList<BookInfo> allBooks=dao.getAllBooks();
    	
    	String bookJson = gson.toJson(allBooks);
    	System.out.println(bookJson);
    	for(int i=0;i<allBooks.size();i++){
    		bookInfo=allBooks.get(i);
    		
    	jaxbObjectToXML(bookInfo, i);
    	}
    	
    	
    }
    
    private static void jaxbObjectToXML(BookInfo bookInfo, int i){
    String FILE_NAME=String.valueOf(i)+"jaxb-bookinfo.xml";
    try{
     	 JAXBContext context=JAXBContext.newInstance(BookInfo.class);
         Marshaller m=context.createMarshaller();
         m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
         m.marshal(bookInfo, new File(FILE_NAME));
     } catch(JAXBException e){
    	 e.printStackTrace();
     }
    }

}
