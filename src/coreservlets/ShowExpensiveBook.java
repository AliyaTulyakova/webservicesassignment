package coreservlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ShowExpensiveBook
 */
@WebServlet("/ShowExpensiveBook")
public class ShowExpensiveBook extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BookInfo bookInfo=BookInfoUtils.getExpensiveBook();
		String address;
		if(bookInfo==null){
			request.setAttribute("bookInfo", bookInfo);
			address="/WEB-INF/results/notfound-bookinfo.jsp";
		} else{
			request.setAttribute("bookInfo", bookInfo);
			address="/WEB-INF/results/show-bookinfo.jsp";
		}
		RequestDispatcher dispatcher=
				request.getRequestDispatcher(address);
		dispatcher.forward(request, response);
		}
	}

	


