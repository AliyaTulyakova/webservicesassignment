package coreservlets;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="booksInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class BooksInfo {
	
	public BooksInfo(){
	}
	
	@XmlElement(name="bookInfo")
	private List<BookInfo> booksInfo;
	public BooksInfo(List<BookInfo> inBooksInfo){
		booksInfo=inBooksInfo;
	}
	public List<BookInfo> getBooksInfo(){
		return booksInfo;
	}
	
	public void setBooksInfo(List<BookInfo> inBooksInfo){
		booksInfo=inBooksInfo;
	}

}
