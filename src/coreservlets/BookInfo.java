package coreservlets;
import java.util.*;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/*
 * This class stores informstion about BookInfo
 * @author Aliya Tulyakova
 * 
 */
@XmlRootElement(name="bookInfo")
//@XmlAccessorType (XmlAccessType.FIELD)
@XmlType(propOrder={"bookId","bookName","bookGenre","bookAuthor","bookPrice","bookAvailability"})
public class BookInfo {

	
	//Create attributes 
	private String bookId;
	private String bookName;
	private String bookGenre;
	private String bookAuthor;
	private double bookPrice;
	private String bookAvailability;
	
	//Constructor
	public BookInfo(){}
	//Constructor to initial the properties
	public BookInfo(String bookId, String bookName, String bookGenre, String bookAuthor, double bookPrice, String bookAvailability){
		/*setBookId(bookId);
		setBookName(bookName);
		setBookGenre(bookGenre);
		setBookAuthor(bookAuthor);
		setBookPrice(bookPrice);
		setBookAvailability(bookAvailability);*/
		
		this.bookId=bookId;
		this.bookName=bookName;
		this.bookGenre=bookGenre;
		this.bookAuthor=bookAuthor;
		this.bookPrice=bookPrice;
		this.bookAvailability=bookAvailability;
	}

	//Create methods
	
	/*
	 * Read book's id
	 * @return Returns book's id
	 */
    @XmlAttribute
	public String getBookId() {
		return bookId;
	}

    /*
	 * The method allows to write an ID
	 */
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	/*
	 * Read book's name
	 * @return Returns book's name
	 */
	public String getBookName() {
		return bookName;
	}

	/*
	 * The method allows to write a Name 
	 */
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	/*
	 * Read book's genre
	 * @return Returns book's genre
	 */
	public String getBookGenre() {
		return bookGenre;
	}

	/*
	 * The method allows to write a Genre 
	 */
	public void setBookGenre(String bookGenre) {
		this.bookGenre = bookGenre;
	}

	/*
	 * Read book's author
	 * @return Returns book's author
	 */
	public String getBookAuthor() {
		return bookAuthor;
	}

	/*
	 * The method allows to write an author 
	 */
	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	/*
	 * Read book's price
	 * @return Returns book's price
	 */
	public double getBookPrice() {
		return bookPrice;
	}

	/*
	 * The method allows to write price
	 */
	public void setBookPrice(double bookPrice) {
		this.bookPrice = bookPrice;
	}

	/*
	 * Read book's availabiliyy
	 * @return Returns book's availability
	 */
	public String getBookAvailability() {
		return bookAvailability;
	}

	/*
	 * The method allows to write availability
	 */
	public void setBookAvailability(String bookAvailability) {
		this.bookAvailability = bookAvailability;
	}

	
	public String toString(){
		//@return This method gegs bookinfo information
		return "BookInfo [bookId=" + bookId + ", bookName=" + bookName + ", bookGenre=" +
		bookGenre + ", bookAuthor=" + bookAuthor + ", bookPrice=" + bookPrice + ", bookAvailability=" + bookAvailability + "]";
		
	}
}
