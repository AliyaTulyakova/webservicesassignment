package coreservlets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import com.book.dao.BookService;



public class BookInfoUtils  {
	
	private Connection c;
	
	
	private static Map<String,BookInfo> sampleBooks;
	
	static {
		sampleBooks=new LinkedHashMap<String,BookInfo>();
		sampleBooks.put("K1202", new BookInfo("K1202","The Girl on the train","Crime&Mystery","Paula Hawkings", 5.99,"in stock"));
		sampleBooks.put("F4402", new BookInfo("F4402","Caraval","Fantasy","Stephanie Garber",9.99,"in stock"));
		sampleBooks.put("G2489", new BookInfo("G2489","The Gustov Sonata","Modern Fiction","Rose Tremain", 6.99,"in stock"));
		sampleBooks.put("M1866", new BookInfo("M1866","The Midnight Gang","Humour","David Walliams", 6.49,"in stock"));
		sampleBooks.put("G1313", new BookInfo("G1313","Who Let the God's Out?","Fantasy","Maz Evans", 5.49,"in stock"));
		sampleBooks.put("D5423", new BookInfo("D5423","Days Without End","Modern Fiction","Sebastian Barry", 6.99,"in stock"));
		sampleBooks.put("M1108", new BookInfo("M1108","Moun!","Romance","Jilly Cooper", 3.99,"in stock"));
		sampleBooks.put("E6402", new BookInfo("E6402","Exit West","Modern Fiction","Mohsin Hamid",11.99,"in stock"));
		sampleBooks.put("B0201", new BookInfo("B0201","Border","Travel writing","Kapka Kassabova",14.99,"in stock"));
		sampleBooks.put("I1607", new BookInfo("I1607","Imagine Me Gone","Modern Fiction","Adam Haslett",6.99,"in stock"));
		sampleBooks.put("A1034", new BookInfo("A1034","Assasin's Fate","Fantasy","Robin Hobb",25.00,"in stock"));
		sampleBooks.put("L0416", new BookInfo("L0416","A Line Made By Walking","Modern Fiction","Sara Baume",9.99,"in stock"));
		sampleBooks.put("C1906", new BookInfo("C1906","Conclave","Historcal Fiction","Robert Harris",12.99,"in stock"));
		sampleBooks.put("C3617", new BookInfo("C3617","Crisis","Thrillers","Frank Gardner",6.49,"in stock"));
		sampleBooks.put("E0400", new BookInfo("E0400","Cold Earth","Modern Fiction","Ann Cleaves",5.99,"in stock"));
		sampleBooks.put("D2133", new BookInfo("D2133","Dear Amy","Thrillers","Helen Callaghan",5.99,"in stock"));
		sampleBooks.put("N3416", new BookInfo("N3416","Nineteen eighty-four","Modern Fiction","George Orwell",6.99,"in stock"));
		sampleBooks.put("K1202", new BookInfo("K1202","The Girl on the train","Crime&Mystery","Paula Hawkings", 5.99,"in stock"));
		sampleBooks.put("F4402", new BookInfo("F4402","Caraval","Fantasy","Stephanie Garber",9.99,"in stock"));
		sampleBooks.put("A4811", new BookInfo("A4811","Down Under","Travel writing","Bill Bryson",6.99,"in stock"));
	}
  
	public static Map<String, BookInfo> getSampleBooks(){
		return(sampleBooks);
	}
	
   /*
    * 
    */
	
	public List<BookInfo> getAllBooks(){
		List<BookInfo> books=(List<BookInfo>) getSampleBooks();
		return books;
		
	}
	/*
	 * Given a name, returns either the corresponding Bookinfo object
	 * or null. 
	 */
	
	public static BookInfo getBookInfo(String bookId){
		if(bookId==null){
			bookId="not found";
		}
		return(sampleBooks.get(bookId.toUpperCase()));
	}
	
	/*
	 * Given a name, returns either the corresponding BookInfo object
	 * or a new BookInfo object with "Not Found" 
	 */
	
	public static BookInfo getBookInfoOrDefault(String bookId){
		BookInfo bookInfo=getBookInfo(bookId);
		String notfound="not found";
		if(bookInfo==null){
			bookInfo=new BookInfo(bookId, notfound, notfound, notfound, 0, notfound);
		}
		return(bookInfo);
	}
	
	/*
	 * Given a book name and an author's name, returns either
	 * the corresponding BookInfo object or null. Assumes that
	 * there are no two books with the same name and author.
	 */
	
	public static BookInfo getNamedBookInfo(String bookName, String bookAuthor){
		Collection<BookInfo> bookInfo=getSampleBooks().values();
		
		for(BookInfo b:bookInfo){
		if((b.getBookName().equalsIgnoreCase(bookName))&&
				(b.getBookAuthor().equalsIgnoreCase(bookAuthor))){
			return b;
		}
		}
		return(null);
	}
	
/**Returns the book info with the highest price. */
	
	public static BookInfo getExpensiveBook(){
		Map<String,BookInfo> bookInfo=getSampleBooks();
		double maxPrice=-Double.MAX_VALUE;
		BookInfo expensivebook=null;
		for(BookInfo b:bookInfo.values()){
			if(b.getBookPrice()>maxPrice){
				maxPrice=b.getBookPrice();
				expensivebook=b;
			}
		}
		return(expensivebook);
	}
/**Returns the two books with the highest price. */
	
public static BookInfo[] getTwoExpensiveBooks(){
	Map<String,BookInfo> bookInfo=getSampleBooks();
	double maxPrice=-Double.MAX_VALUE;
	BookInfo firstBook=null;
	for(BookInfo b:bookInfo.values()){
		if(b.getBookPrice()>maxPrice){
			maxPrice=b.getBookPrice();
			firstBook=b;
		}
	}
		
	maxPrice=-Double.MAX_VALUE;
	BookInfo secondBook=null;
	for(BookInfo b:bookInfo.values()){
		if((b.getBookPrice()>maxPrice)&&
			(b.getBookPrice()<firstBook.getBookPrice())){
			maxPrice=b.getBookPrice();
		}
	}
	BookInfo[] twoexpensive={firstBook, secondBook};
	return(twoexpensive);
	
	}

private BookInfoUtils(){}
}
