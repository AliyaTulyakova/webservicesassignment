package coreservlets;
import java.io.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ShowBookInfo extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException{
		String bookId=request.getParameter("bookId");
		if((bookId==null)||(bookId.trim().equals(""))){
			bookId="<i>missing-bookId</i>";
		}
	BookInfo bookInfo=BookInfoUtils.getBookInfo(bookId);
	String address;
	if(bookInfo==null){
		request.setAttribute("bookId", bookId);
		address="/WEB-INF/results/notfound-bookinfo.jsp";
	} else{
		request.setAttribute("bookInfo", bookInfo);
		address="/WEB-INF/results/show-bookinfo.jsp";
	}
	RequestDispatcher dispatcher=
			request.getRequestDispatcher(address);
	dispatcher.forward(request, response);
	}

}
