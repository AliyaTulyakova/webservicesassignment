package coreservlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GetAllBooks
 */
@WebServlet("/GetAllBooks")
public class GetAllBooks extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control", "no-cache");
	    response.setHeader("Pragma", "no-cache");
		bookDAO dao=new bookDAO();
		List<BookInfo> booksInfo=null;
		booksInfo=dao.getAllBooks();
		String format=request.getParameter("format");
		String outputPage;
		request.setAttribute("booksInfo", booksInfo);
		
		if("xml".equals(format)){
			response.setContentType("text/xml");
			outputPage="/WEB-INF/viewers/books-xml.jsp";
		} else if ("json".equals(format)){
			response.setContentType("application/json");
			outputPage="/WEB-INF/viewers/books-json.jsp";
		} else {
			response.setContentType("text/plain");
			outputPage="/WEB-INF/viewers/books-string.jsp";}
		
		RequestDispatcher dispatcher=request.getRequestDispatcher(outputPage);
		dispatcher.forward(request, response);
		
		
		
		}
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
