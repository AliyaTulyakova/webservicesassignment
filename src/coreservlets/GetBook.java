package coreservlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GetBook
 */
@WebServlet("/GetBook")
public class GetBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetBook() {
        super();   
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		bookDAO bdao=new bookDAO();
		String searchBookname=request.getParameter("bookName");
		BookInfo sList=new BookInfo();
		sList=bdao.getBookByName(searchBookname);
		String outputPage;
		RequestDispatcher dispatcher;
		if(sList==null){
			request.setAttribute("bookName", searchBookname);
			outputPage="/WEB-INF/viewers/unknown-book.jsp";
			dispatcher=request.getRequestDispatcher(outputPage);
		} else{
			request.setAttribute("sList", sList);
			outputPage="/WEB-INF/viewers/show-book.jsp";
			dispatcher=request.getRequestDispatcher(outputPage);
		}
		dispatcher.forward(request, response);
		 
	
	     
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
