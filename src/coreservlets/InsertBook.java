package coreservlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InsertBook
 */
@WebServlet("/InsertBook")
public class InsertBook extends HttpServlet {
	private static final long serialVersionUID = 1L; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertBook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//BookInfo bookInfo=new BookInfo();
		//bookDAO dao=new bookDAO();
		String inputPage;
		
		/*
		bookInfo.setBookId(request.getParameter("bookId"));
		bookInfo.setBookName(request.getParameter("bookName"));
		bookInfo.setBookGenre(request.getParameter("bookGenre"));
		bookInfo.setBookAuthor(request.getParameter("bookAuthor"));
		//bookInfo.setBookPrice(Double.parseDouble(request.getParameter("bookPrice")));
		bookInfo.setBookAvailability(request.getParameter("bookAvailability"));*/
		/*
		String bookId=request.getParameter("bookId");
		String bookName=request.getParameter("bookName");
		String bookGenre=request.getParameter("bookGenre");
		String bookAuthor=request.getParameter("bookAuthor");
	    double bookPrice=Double.parseDouble(request.getParameter("bookPrice"));
		String bookAvailability=request.getParameter("bookAvailability");*/
		
		//BookInfo bookInfo=new BookInfo();
		/*
		dao.insertBookInfo(bookInfo);
		request.setAttribute("bookInfo", bookInfo);*/
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request,response);
		String inputPage;
        BookInfo bookInfo=new BookInfo();
		bookDAO dao=new bookDAO();
		response.setContentType("text/html");
		bookInfo.setBookId(request.getParameter("bookId"));
		bookInfo.setBookName(request.getParameter("bookName"));
		bookInfo.setBookGenre(request.getParameter("bookGenre"));
		bookInfo.setBookAuthor(request.getParameter("bookAuthor"));
		//bookInfo.setBookPrice(Double.parseDouble(request.getParameter("bookPrice")));
		bookInfo.setBookAvailability(request.getParameter("bookAvailability"));
		dao.insertBookInfo(bookInfo);
		request.setAttribute("bookInfo", bookInfo);
		inputPage="/WEB-INF/viewers/insertbook.jsp";
		RequestDispatcher dispatcher=request.getRequestDispatcher(inputPage);
	    dispatcher.forward(request, response);
		
		
	}

}
