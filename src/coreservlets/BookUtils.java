package coreservlets;

import java.util.*;

/*
 * Static methods for retrieving information about books.
 */

public class BookUtils {
	
	private static Map<String,BookInfo> bestSellingBooks=
			new HashMap<String,BookInfo>();
	
	public static Map<String,BookInfo> getBookInfoMap(){
		return(bestSellingBooks);
	}
	
private static void storeBookInfo(String bookId, String bookName, String bookGenre, String bookAuthor, double bookPrice, String bookAvailability){
	
	BookInfo bookInfo=new BookInfo(bookId,bookName,bookGenre,bookAuthor,bookPrice,bookAvailability);
	bookName=bookName.toUpperCase();
	bestSellingBooks.put(bookName, bookInfo);
}

public static BookInfo getBook(String bookName){
	bookName=bookName.toUpperCase();
	return(bestSellingBooks.get(bookName));
}

public static BookInfo getBookOrDefault(String bookName){
	BookInfo bookInfo=getBook(bookName);
	if(bookInfo==null){
		bookName=bookName+"(unknown)";
		bookInfo=new BookInfo("",bookName,"","",0,"");
	}
	return (bookInfo);
}

private static Map<String,String[]> bookTypeMap;

public static Map<String,String[]> getBookTypeMap(){
	return(bookTypeMap);
}

static {
	storeBookInfo("G2489","The Gustov Sonata","Modern Fiction","Rose Tremain", 6.99,"in stock");
	storeBookInfo("M1866","The Midnight Gang","Humour","David Walliams", 6.49,"in stock");
	storeBookInfo("G1313","Who Let the God's Out?","Fantasy","Maz Evans", 5.49,"in stock");
	storeBookInfo("D5423","Days Without End","Modern Fiction","Sebastian Barry", 6.99,"in stock");
	storeBookInfo("M1108","Moun!","Romance","Jilly Cooper", 3.99,"in stock");
	storeBookInfo("E6402","Exit West","Modern Fiction","Mohsin Hamid",11.99,"in stock");
	storeBookInfo("B0201","Border","Travel writing","Kapka Kassabova",14.99,"in stock");
	storeBookInfo("I1607","Imagine Me Gone","Modern Fiction","Adam Haslett",6.99,"in stock");
	storeBookInfo("A1034","Assasin's Fate","Fantasy","Robin Hobb",25.00,"in stock");
	storeBookInfo("L0416","A Line Made By Walking","Modern Fiction","Sara Baume",9.99,"in stock");
	storeBookInfo("C1906","Conclave","Historcal Fiction","Robert Harris",12.99,"in stock");
	storeBookInfo("C3617","Crisis","Thrillers","Frank Gardner",6.49,"in stock");
	storeBookInfo("E0400","Cold Earth","Modern Fiction","Ann Cleaves",5.99,"in stock");
	storeBookInfo("D2133","Dear Amy","Thrillers","Helen Callaghan",5.99,"in stock");
	storeBookInfo("N3416","Nineteen eighty-four","Modern Fiction","George Orwell",6.99,"in stock");
	storeBookInfo("K1202","The Girl on the train","Crime&Mystery","Paula Hawkings", 5.99,"in stock");
	storeBookInfo("F4402","Caraval","Fantasy","Stephanie Garber",9.99,"in stock");
	storeBookInfo("A4811","Down Under","Travel writing","Bill Bryson",6.99,"in stock");
	storeBookInfo("K1202","The Girl on the train","Crime&Mystery","Paula Hawkings", 5.99,"in stock");
	storeBookInfo("F4402","Caraval","Fantasy","Stephanie Garber",9.99,"in stock");
	storeBookInfo("A4811","Down Under","Travel writing","Bill Bryson",6.99,"in stock");
    bookTypeMap=makeBookMap();

}

 private static Map<String,String[]> makeBookMap(){
	 String[] fiveBestSellingTitles=
		 {"Who Let the God's Out?","Days Without End","The Midnight Gang","Moun!","The Gustov Sonata"};
	 String[] fiveBestNewReviewed=
		 {"Exit West","Border","Imagine Me Gone","Assasin's Fate","A Line Made By Walking"};	 
	 String[] fiveBestFictionBooks=
		 {"Nineteen eighty-four","Cold Earth","Crisis","Conclave","Dear Amy"};	 
     
	 Map<String,String[]> bookTypeMap=new HashMap<String,String[]>();
	 bookTypeMap.put("5-selling-titles", fiveBestSellingTitles);
	 bookTypeMap.put("5-new-reviewed", fiveBestNewReviewed);
	 bookTypeMap.put("5-fiction-books", fiveBestFictionBooks);
	 return(bookTypeMap);
 
 }
 
   public static List<BookInfo> findBooks(String bookType){
	   String[] bookNames=bookTypeMap.get(bookType);
	   if(bookNames==null){
		   String[] twoBooks={"Who Let the God's Out?","Days Without End"};
	       bookNames=twoBooks;
	   }
	   List<BookInfo> books=new ArrayList<BookInfo>();
	   for(String bookName:bookNames){
		   books.add(getBook(bookName));
	   }
	   return(books);
   }
   
   private BookUtils(){}
}
