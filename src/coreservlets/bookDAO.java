package coreservlets;
/*
 * This is a data access object to provide interface
 * @author Aliya
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.book.dao.BookService;
public class bookDAO implements BookService {
    
	/*
	 * Create a BookInfo object, Connection object, Statement object, ResultSet object
	 * @param bookInfo - BookInfo object
	 * @param c - Connection object
	 * @param s - Statement object
	 * @param r - ResultSet object 
	 */
	BookInfo bookInfo=null;
	Connection c=null;
	Statement s=null;
	ResultSet r=null;
	
   public static final String username="tulyakoa";
   public static final String password="Qwegcade6";
   
   public bookDAO(){}
   
   /*
    * Open Database Connection
    */
   public void openConnection(){
	   try{
		 Class.forName("com.mysql.jdbc.Driver").newInstance();
	   } catch(Exception e){
		   System.out.println(e);
	   }
	   try {
		   
		   c=DriverManager.getConnection("jdbc:mysql://mudfoot.doc.stu.mmu.ac.uk:3306/tulyakoa?user=tulyakoa&password=Qwegcade6");
		   s=c.createStatement();
	   } catch(SQLException se){
		   System.out.println(se);
	   }
   }
   
   /*
    * Close connection
    */
   public void closeConnection(){
	   try {
		   c.close();
	   } catch(SQLException e){
		   e.printStackTrace();
	   }
   }

    /*
     * Retrieve all books, create ArrayList, open connection
     * @return Returns ArrayList books
     */
   @Override
   public ArrayList<BookInfo> getAllBooks() {
   	ArrayList<BookInfo> books=new ArrayList<BookInfo>();
   	openConnection();
   	try{
   	Statement s=c.createStatement();
   	ResultSet rs=s.executeQuery("select * from book");
   	while(rs.next()){
   		BookInfo bookInfo=new BookInfo();
   	    bookInfo.setBookId(rs.getString("bookId"));
   	    bookInfo.setBookName(rs.getString("bookName"));
   	    bookInfo.setBookGenre(rs.getString("bookGenre"));
   	    bookInfo.setBookAuthor(rs.getString("bookAuthor"));
   	    bookInfo.setBookPrice(rs.getDouble("bookPrice"));
   	    bookInfo.setBookAvailability(rs.getString("bookAvailability"));
   	    books.add(bookInfo); //can change
   	}rs.close();
   	s.close();
   	} catch(SQLException e){
   		e.printStackTrace();
   	} return books;
   }


   
   /*
    * Get next book, create thisBookInfo object
    * @throws SQLException
    */
   private BookInfo getNextBook(ResultSet r){
	   BookInfo thisBookInfo=null;
	   try{
		   thisBookInfo=new BookInfo(
				   r.getString("bookId"),
				   r.getString("bookName"),
				   r.getString("bookGenre"),
				   r.getString("bookAuthor"),
				   r.getDouble("bookPrice"),
				   r.getString("bookAvailability"));
	   }  catch (SQLException e){
		   e.printStackTrace();
	   }
	   return thisBookInfo;
   }
  
  /*
   * Retrieve bookInfo by Name
   * @return Returns a bookinfo by name
   * @throws SQLException
   */
   public BookInfo getBookByName(String bookName){
	   openConnection(); //open connection
	   BookInfo bookInfo=new BookInfo(); //create bookinfo object
	   
	   try{
		  String sqlselect="select * from book where bookName=?";
		  PreparedStatement pstmt=c.prepareStatement(sqlselect);
		  pstmt.setString(1,bookName);
		  ResultSet rs=pstmt.executeQuery();
		  while(rs.next()){
			  bookInfo.setBookId(rs.getString("bookId"));
			  bookInfo.setBookName(rs.getString("bookName"));
			  bookInfo.setBookGenre(rs.getString("bookGenre"));
			  bookInfo.setBookAuthor(rs.getString("bookAuthor"));
			  bookInfo.setBookPrice(rs.getDouble("bookPrice"));
			  bookInfo.setBookAvailability(rs.getString("bookAvailability"));
		  }
		  rs.close();
		  pstmt.close();
	   } catch(SQLException e){
		   e.printStackTrace();
	   }return bookInfo;
   }
   /*
    * Insert Bookinfo into database
    * 
    * @param bookInfo BookInfo object
    * @throws Exception
    */
   public void insertBookInfo(BookInfo bookInfo) {
		try{
		Class.forName("com.mysql.jdbc.Driver");
		c=DriverManager.getConnection("jdbc:mysql://mudfoot.doc.stu.mmu.ac.uk:3306/tulyakoa?user=tulyakoa&password=Qwegcade6");
		c.setAutoCommit(false);
		System.out.println("Insert operation - database successfully opened");
		
	Scanner sc=new Scanner(System.in);
	String id=sc.nextLine();
	String name=sc.nextLine();
	String genre=sc.nextLine();
	String author=sc.nextLine();
	double price=sc.nextDouble();
	String availability=sc.nextLine();
		
		PreparedStatement pstmt=c.prepareStatement("insert into book (bookId,bookName,bookGenre,bookAuthor,bookPrice,bookAvailability) values (?,?,?,?,?,?)");
	pstmt.setString(1, id);
	pstmt.setString(2, name);
	pstmt.setString(3, genre);
	pstmt.setString(4, author);
	pstmt.setDouble(5, price);
    pstmt.setString(6, availability);
	pstmt.executeUpdate();
		c.commit();
		pstmt.close();
		c.close();
		}catch(Exception e){
			System.err.println(e.getMessage());
			System.exit(0);
			
		}System.out.println("Insert operation successfully done");
		
	}

   /*
    * Delete BookInfo by ID
    * @param id - ID of bookinfo
    * @throws Exception
    */
public void deleteBookInfo() {
	try{
	Class.forName("com.mysql.jdbc.Driver");
	c=DriverManager.getConnection("jdbc:mysql://mudfoot.doc.stu.mmu.ac.uk:3306/tulyakoa?user=tulyakoa&password=Qwegcade6");
	c.setAutoCommit(false);
	BookInfo bookInfo=new BookInfo();
	System.out.println("Delete operation - database successfully opened");
	System.out.println("Enter book's ID to delete");
	Scanner sc=new Scanner(System.in);
	String id=sc.nextLine();
	PreparedStatement pstmt=c.prepareStatement("delete from book where bookId=?");
	pstmt.setString(1, id);
	pstmt.executeUpdate();
	c.commit();
	pstmt.close();
	c.close();
	}catch(Exception e){
		System.err.println(e.getMessage());
		System.exit(0);
		
	}System.out.println("Delete operation successfully done");
	
}

   
   
   
   
}