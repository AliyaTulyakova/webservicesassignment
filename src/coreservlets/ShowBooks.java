package coreservlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

/*
 * Servlet that uses MVC to send city info in XML, JSON, and text formats
 */
public class ShowBooks extends HttpServlet {
	
	@Override
  public void doGet(HttpServletRequest request,HttpServletResponse response)
   throws ServletException, IOException {
		
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	String bookType=request.getParameter("bookType");
	
	
	List<BookInfo> books=BookUtils.findBooks(bookType);
	request.setAttribute("books", books);
	String format=request.getParameter("format");
	String outputPage;
	if("xml".equals(format)){
		response.setContentType("text/xml");
		outputPage="/WEB-INF/results/book-xml.jsp";
	} else if("json".equals(format)){
		response.setContentType("application/json");
		outputPage="/WEB-INF/results/book-json.jsp";
	} else {
		response.setContentType("text/plain");
		outputPage="/WEB-INF/results/book-string.jsp";
	}
	RequestDispatcher dispatcher=request.getRequestDispatcher(outputPage);
	dispatcher.include(request,response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
	  throws ServletException, IOException {
		doGet(request,response);
	}
	
	
}
