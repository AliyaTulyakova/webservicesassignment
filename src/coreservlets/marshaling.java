package coreservlets;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class marshaling {
	
	
	
    public static void main(String[] args) throws JAXBException, IOException{
    	final BooksInfo booksInfo=new BooksInfo();
		bookDAO dao=new bookDAO();
		BookInfo bookInfo=new BookInfo();
		ArrayList<BookInfo> allBooks=dao.getAllBooks();
		booksInfo.setBooksInfo(allBooks);
		
		for (int i=0;i<allBooks.size();i++){
			bookInfo=allBooks.get(i);
			System.out.println("All Books "+i+" "+
					bookInfo.toString());
			marshaling.marshalingXML(bookInfo);
		}
		
    
    }
    
	private static void marshalingXML(BookInfo bookInfo) throws JAXBException, IOException{
		JAXBContext jaxbContext=JAXBContext.newInstance(BookInfo.class);
		Marshaller jaxbMarshaller=jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
		jaxbMarshaller.marshal(bookInfo, System.out);
		
		jaxbMarshaller.marshal(bookInfo, new FileWriter("booksinfo.xml", true));
	}

}
