package com.book.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import coreservlets.BookInfo;

public interface BookService {
   
	public void insertBookInfo(BookInfo bookInfo);
	public ArrayList<BookInfo> getAllBooks();
	public BookInfo getBookByName(String bookName);
	public void deleteBookInfo();
	
}
