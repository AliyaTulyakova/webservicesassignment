

$(function() {
	$("#getStringButton").click(stringBookInfoTable);
	$("#getXMLButton").click(xmlBookInfoTable);
	$("#getJsonButton").click(jsonBookInfoTable);
});

function xmlBookInfoTable(){
	var workingRegion="#working";
	var resultRegion="#xml-book-table";
	$(resultRegion).html("");
	$(workingRegion).show();
	$.ajax({
		url: "GetAllBooks?format=xml",
		success: function(xml) {$(resultRegion).html(xml);},
	    complete: function() {$(workingRegion).hide();}
	});
}

function showXmlBookInfo(booksInfo){
	
	$("#xml-book-table").html(xml);
	
}

function jsonBookInfoTable(){
	var workingRegion="#working";
	var resultRegion="#json-book-table";
	$(resultRegion).html("");
	$(workingRegion).show();
	$.ajax({url: "GetAllBooks?format=json",
		    success: function(json) {$(resultRegion).html(json);},
		    complete: function() {$(workingRegion).hide();}
		    });
	}


function showJsonBookInfo(jsonData){
	var list=makeList(jsonData.bookId, jsonData.bookName);
	$("#json-book-table").html(list);
}

function stringBookInfoTable(){
	var workingRegion="#working";
	var resultRegion="#string-book-table";
	$(resultRegion).html("");
	$(workingRegion).show();
	$.ajax({
		url: "GetAllBooks?format=string",
		success: function(text) {$(resultRegion).html(text);},
		complete: function() {$(workingRegion).hide();}
	});
	
}

function showStringBookInfo(string) {
	
	$("#string-book-table").html(string);
}


function bookInfoTable(bookTypeField, formatField, resultRegion){
	var address="GetAllBooks";
	var bookInfoType=getValue(bookTypeField);
	var format=getValue(formatField);
	var data="bookInfoType="+bookInfoType+
	     "&format="+format;
	var responseHandler=findHandler(format);
	ajaxPost(address, data,
			function(request){
		responseHandler(request, resultRegion)
	});
}


function findHandler(format){
	if(format=="xml"){
		return(showXmlBookInfo);
	} else if(format=="json"){
		return(showJsonBookInfo);
	} else {
		return(showStringBookInfo);
	}
}