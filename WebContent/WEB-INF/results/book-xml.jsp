<?xml version="1.0" encoding="UTF-8"?>
<books>
  <headings>
    <heading>ID</heading>
    <heading>Name</heading>
    <heading>Genre</heading>
    <heading>Author</heading>
    <heading>Price</heading>
    <heading>Availability</heading>
  </headings>
  <bookInfo>
     <bookId>${books[0].bookId}</bookId>
     <bookName>${books[0].bookName}</bookName>
     <bookGenre>${books[0].bookGenre}</bookGenre>
     <bookAuthor>${books[0].bookAuthor}</bookAuthor>
     <bookPrice>${books[0].bookPrice}</bookPrice>
     <bookAvailability>${books[0].bookAvailability}</bookAvailability>
  </bookInfo>
   <bookInfo>
     <bookId>${books[1].bookId}</bookId>
     <bookName>${books[1].bookName}</bookName>
     <bookGenre>${books[1].bookGenre}</bookGenre>
     <bookAuthor>${books[1].bookAuthor}</bookAuthor>
     <bookPrice>${books[1].bookPrice}</bookPrice>
     <bookAvailability>${books[1].bookAvailability}</bookAvailability>
  </bookInfo>
   <bookInfo>
     <bookId>${books[2].bookId}</bookId>
     <bookName>${books[2].bookName}</bookName>
     <bookGenre>${books[2].bookGenre}</bookGenre>
     <bookAuthor>${books[2].bookAuthor}</bookAuthor>
     <bookPrice>${books[2].bookPrice}</bookPrice>
     <bookAvailability>${books[2].bookAvailability}</bookAvailability>
  </bookInfo>
   <bookInfo>
     <bookId>${books[3].bookId}</bookId>
     <bookName>${books[3].bookName}</bookName>
     <bookGenre>${books[3].bookGenre}</bookGenre>
     <bookAuthor>${books[3].bookAuthor}</bookAuthor>
     <bookPrice>${books[3].bookPrice}</bookPrice>
     <bookAvailability>${books[3].bookAvailability}</bookAvailability>
  </bookInfo>
   <bookInfo>
     <bookId>${books[4].bookId}</bookId>
     <bookName>${books[4].bookName}</bookName>
     <bookGenre>${books[4].bookGenre}</bookGenre>
     <bookAuthor>${books[4].bookAuthor}</bookAuthor>
     <bookPrice>${books[4].bookPrice}</bookPrice>
     <bookAvailability>${books[4].bookAvailability}</bookAvailability>
  </bookInfo>

</books>