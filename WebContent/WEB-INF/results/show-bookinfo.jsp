<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Book Info</title>

</head>
<body>
<div align="center">
<table border="1">
  <tr><th class="title">Book Info</th></tr>
</table>
<p/>
<ul>
  <li>Book ID: ${bookInfo.bookId}</li>
  <li>Book Name: ${bookInfo.bookName}</li>
  <li>Book Genre: ${bookInfo.bookGenre}</li>
<li>Book Author: ${bookInfo.bookAuthor}</li>
<li>Book Price: ${bookInfo.bookPrice}</li>
<li>Book Availability: ${bookInfo.bookAvailability}</li>
</ul>
</div></body></html>