<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
 <title></title>
 </head>
 <body>
    <table border="1"  >
       <tr>
          <th>BookID</th>
          <th>Name</th>
          <th>Genre</th>
          <th>Author</th>
          <th>Price</th>
          <th>Availability</th>
       </tr>
       <c:forEach items="${books}" var="bookInfo" >
          <tr>
              <td>${books.bookID}</td>
              <td>${books.bookName}</td>
              <td>${books.bookGenre}</td>
              <td>${books.bookAuthor}</td>
              <td>${books.bookPrice}</td>
              <td>${books.bookAvailability}</td>
             
          </tr>
       </c:forEach>
    </table>
 </body>
</html>