<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert new book</title>
</head>
<body>
  
        <input type="text" name="bookId" value="<c:out value="${bookInfo.bookId}"/>" placeholder="Book ID"/>
        <input type="text" name="bookName" value="<c:out value="${bookInfo.bookName}"/>" placeholder="Name"/>
        <input type="text" name="bookGenre" value="<c:out value="${bookInfo.bookGenre}"/>" placeholder="Genre"/>
        <input type="text" name="bookAuthor" value="<c:out value="${bookInfo.bookAuthor}"/>" placeholder="Author"/>
        <input type="text" name="bookPrice" value="<c:out value="${bookInfo.bookPrice}"/>" placeholder="Price"/>
        <input type="text" name="bookAvailability" value="<c:out value="${bookInfo.bookAvailability}"/>" placeholder="Availability"/>

  
</body>
</html>