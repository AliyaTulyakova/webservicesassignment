<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Book Info</title>
<link rel="stylesheet"
      href="./css/styles.css"
      type="text/css"/>
</head>
<body>
<div align="center">
<table border="5">
  <tr><th class="title">Book Info</th></tr>
</table>
<p/>
<ul>
  <li>Book ID: ${sList.bookId}</li>
  <li>Name:  ${sList.bookName}</li>
  <li>Genre:   ${sList.bookGenre}</li>
  <li>Author:     ${sList.bookAuthor}</li>
  <li>Price:   ${sList.bookPrice}</li>
  <li>Availability: ${sList.bookAvailability}</li>
</ul>
</div></body></html>