<%@ page import="java.util.List" %>
<%@ page import="javax.xml.bind.JAXBContext" %>
<%@ page import="javax.xml.bind.JAXBException" %>
<%@ page import="javax.xml.bind.Marshaller" %>
<%@ page import="coreservlets.BookInfo" %>
<%@ page import="coreservlets.BooksInfo" %>

<%
BooksInfo booksInfo= new BooksInfo((List<BookInfo>) request.getAttribute("booksInfo"));
 try{
	 JAXBContext jaxbContext = JAXBContext.newInstance(BooksInfo.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(booksInfo, out);
		
 } catch(JAXBException e){
	 e.printStackTrace();
 }
%>