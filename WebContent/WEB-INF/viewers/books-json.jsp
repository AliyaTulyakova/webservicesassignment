<%@ page import="java.util.List" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="coreservlets.BookInfo" %>

<%
List<BookInfo> booksInfo = (List<BookInfo>) request.getAttribute("booksInfo");

Gson gson = new Gson();

String jsonInString = gson.toJson(booksInfo);
response.getWriter().println(jsonInString);
%>
